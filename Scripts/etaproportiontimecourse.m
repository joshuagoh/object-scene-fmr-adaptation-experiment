function [s To Tb]= etaproportiontimecourse(trials)

% Set parameters
nbin=75; % 1500 ms stim duration
tperbin=20; % msec
bin=[1:nbin];
cnames={'OO_1';'OO_2';'OO_3';'OO_4';...
    'ON_1';'ON_2';'ON_3';'ON_4';...
    'NO_1';'NO_2';'NO_3';'NO_4';...
    'NN_1';'NN_2';'NN_3';'NN_4'};

% Begin loops
for i=1:length(trials) % Subject loop
    s(i).nctrials=zeros(length(cnames),1);
    s(i).nctb=zeros(length(cnames),nbin);
    for j=1:length(trials(i).Trials) % Trial loop
        s(i).t(j).nod=zeros(nbin,1); s(i).t(j).nbd=zeros(nbin,1); k=1; b=1;
        while b<(nbin+1) % Bin loop
            while (k<=length(trials(i).Trials(j).d))&&(trials(i).Trials(j).d(k).stimtime<(bin(b)*tperbin)) % Datapoint loop
                if strcmp('O',trials(i).Trials(j).d(k).aoi)==1
                    s(i).t(j).nod(b,1)=s(i).t(j).nod(b,1)+1;
                else
                    s(i).t(j).nbd(b,1)=s(i).t(j).nbd(b,1)+1;
                end
                k=k+1;
            end % End Datapoint loop
            b=b+1;
        end % End Bin loop

        % Sort trial data into condition
        c=1;
        while c<=length(cnames)
            if strcmp(cnames(c),trials(i).Trials(j).cond)==1
                s(i).nctrials(c,1)=s(i).nctrials(c,1)+1;

                % Convert to proportion datapoints and store
                for q=1:nbin
                    if (s(i).t(j).nod(q)+s(i).t(j).nbd(q))==0 % check zero divisors
                        s(i).t(j).pnod(q)=0;
                        s(i).t(j).pnbd(q)=0;
                    else
                        s(i).t(j).pnod(q)=s(i).t(j).nod(q)./(s(i).t(j).nod(q)+s(i).t(j).nbd(q));
                        s(i).t(j).pnbd(q)=s(i).t(j).nbd(q)./(s(i).t(j).nod(q)+s(i).t(j).nbd(q));
                        s(i).nctb(c,q)=s(i).nctb(c,q)+1;
                    end
                end

                s(i).ctpnod(c,s(i).nctrials(c,1),:)=s(i).t(j).pnod;
                s(i).ctpnbd(c,s(i).nctrials(c,1),:)=s(i).t(j).pnbd;
                s(i).cnod(c,s(i).nctrials(c,1))=sum(s(i).t(j).nod); % Number of object datapoints
                s(i).cnbd(c,s(i).nctrials(c,1))=sum(s(i).t(j).nbd); % Number of background datapoints

            end
            c=c+1;
        end % End condition sort loop
    end % End Trial loop

    % Average trial proportions
    for r=1:length(cnames)
        for p=1:nbin
            if s(i).nctb(r,p)==0
                ao(p)=0;
                ab(p)=0;
            else
                ao(p)=sum(s(i).ctpnod(r,:,p))./s(i).nctb(r,p);
                ab(p)=sum(s(i).ctpnbd(r,:,p))./s(i).nctb(r,p);
            end
        end
        s(i).apnod(r,:)=ao';
        s(i).apnbd(r,:)=ab';
    end
end % End Subject loop

for h=1:length(cnames)
    for bins=1:nbin
        for subj=1:length(trials)
            saveo(subj)=s(subj).apnod(h,bins);
            saveb(subj)=s(subj).apnbd(h,bins);
        end
        if length(find(saveo))==0
            To(h,bins)=0;
        else
            To(h,bins)=sum(saveo)/length(find(saveo)); % Compensate mean for zero cells
        end
        if length(find(saveb))==0
            Tb(h,bins)=0;
        else
            Tb(h,bins)=sum(saveb)/length(find(saveb)); % Compensate mean for zero cells
        end
    end
end
