function [trials u output]=gz_analyze_batch(subdir,basedir,method,par1,par2)

% basedir - Project base directory
% method - 'r' radius or 's' saccade
% par1 - saccade velocity threshold or radius threshold
% par2 - fixation duration threshold (ms) if radius method; blank otherwise

subjdir=sprintf('%sSubject_Data/%s',basedir,subdir);
subjfiles=dir(fullfile(subjdir,'*.gazedata'));

fprintf('%s','Processing subject...   ');

for i=1:length(subjfiles)

fprintf('%s',char(sprintf('\b\b\b')));
fprintf('%03d',i);

gfile=sprintf('%s%s',subjdir,subjfiles(i).name)

if (strfind(subjfiles(i).name,'Enc1') ~= 0)
maskdir=sprintf('%sPresentation_Files/omasks1',basedir);
else
maskdir=sprintf('%sPresentation_Files/omasks2',basedir);
end

if method=='r'
[data p report]=gz_analyze_radius(gfile,maskdir,par1,par2);
else
[data p report]=gz_analyze_velocity(gfile,maskdir,par1);
end

trials(i)=struct('Trials',data); u(i)=struct('U',p); output(i)=struct('Output',report);

end

disp('All subjects done!')
