function nd2txt(s)

% s(2)=eos, s(1)=eys, s(4)=wos, s(3)=wys

n=1;

for i=1:length(s)
    for j=1:length(s(i).d)
        m=1;
        for k=1:size(s(i).d(j).cnod,1)
            datao(n,m)=mean(s(i).d(j).cnod(k,:));
            datab(n,m)=mean(s(i).d(j).cnbd(k,:));
            m=m+1;
        end;
        n=n+1;
    end;
end;

fid1=fopen('nod.txt','w');
fid2=fopen('nbd.txt','w');
for r=1:size(datao,1)
    for c=1:size(datao,2)
        fprintf(fid1,'%f\t',datao(r,c));
        fprintf(fid2,'%f\t',datab(r,c));
    end;
    fprintf(fid1,'\n');
    fprintf(fid2,'\n');
end;

fclose(fid1);
fclose(fid2);
