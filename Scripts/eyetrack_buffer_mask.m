% function bufferedmask = eyetrack_buffer_mask(mask,bufferradius)
% returns a new mask buffered smoothly around the edges
% NOTE: requires image toolbox
% 
% parameters:
%	mask:		a bitmap mask (white mask, black background)
%	bufferradius:	radius of buffer around the edge
%
% returns grayscale (xdim,ydim,1) (see below to squeeze color bmp)
% set intensity threshold for what is object: default 255
%
% for example:
%	b = imread('mymask.bmp');
%	
%	% squeeze to two dimensions (drop color info)
%	b = squeeze(b(:,:,1));
%	
%	% buffer the mask
%	bufferedmask = eyetrack_buffer_mask(b,radius);
%	
%	% write to disk
%	imwrite(b,'buffered_mymask.bmp');
%
% last updated: Andy Hebrank Mar 3, 2006

function bufferedmask = eyetrack_buffer_mask(mask,radius)
	% threshold for what color value constitutes the mask object
	maskintensity=255;
	% maxintensity = max(mask(:));
	
	% buffer the whole mask around outside edges--takes care
	% of any edge problems
	newmask = zeros(size(mask,1)+(radius*4),...
			size(mask,2)+(radius*4));
	newmask((radius*2)+1:(radius*2)+size(mask,1),...
		(radius*2)+1:(radius*2)+size(mask,2)) = mask;

	% find masked pixels
	iM = find(newmask==maskintensity);
	
	edges = [];
	% find edges out of masking pixels
	for i=1:length(iM)
		if (get_nsurround(iM(i),newmask,maskintensity)<9)
			edges = [edges iM(i)];
		end
	end

	% make a circle mask
	circlemask = maskintensity*get_circle(radius);

	% at every point on an edge, add the circle to that area
	for i=1:length(edges)
		[i j] = ind2sub(size(newmask),edges(i));
		newmask(i-radius:i+radius,j-radius:j+radius) = ...
			max(newmask(i-radius:i+radius,j-radius:j+radius),...
			    circlemask);
	end
	
	% remove the outside buffer
	bufferedmask = newmask((radius*2)+1:(radius*2)+size(mask,1),...
		(radius*2)+1:(radius*2)+size(mask,2));
	
	
	
% END MAIN FUNCTION
	

% returns number of masked pixels around a given pixel
function s = get_nsurround(index,image,y)
	[i j] = ind2sub(size(image),index);
	subm=image(i-1:i+1,j-1:j+1);
	s = length(find(subm==y));

	
% draw a circle on a matrix
function a = get_circle(radius)
	theta_deg = 0:360;
    	theta = pi*(theta_deg/180);
	
	a = zeros(radius*2+1,radius*2+1);
	x0 = radius+1; y0 = radius+1;
	a(x0,y0) = 1;
	
	% create a filled circle in mat
	for r=1:radius
		x = r*cos(theta);
		y = r*sin(theta);
		for i=1:360
			a(round(x0+x(i)),round(y0+y(i)))=1;
		end
	end
	
