function etaptimecourseplot(To,itype)

% Set parameters
x=[1:100:1500];
i=1;
color=[0 0 1; 0 1 0; 1 .8 0; 1 0 0];

for c=1:4
    subplot(2,2,c),
    for p=1:4
        %rcolor=color(c,:)-(color(c,:)*((p-1)*1/5));        
        if (p==1)||(p==3)
            rcolor=color(c,:);
            lw=2;
            if (p==3)
                ls='--';
            else
                ls='-';
            end
        else
            rcolor=color(c,:)-(color(c,:)*(1/2));
            lw=2;
            if (p==4)
                ls='--';
            else
                ls='-';
            end
        end
        plot(x,To(i,:),ls,'Color',rcolor,...
            'LineWidth',lw);
        i=i+1;
        hold on;
    end
    set(gcf,'Color',[1 1 1]);
    set(gca,'YLim',[0 1]);
end;
saveas(gcf,itype,'bmp');
hold off;
close;

clear all