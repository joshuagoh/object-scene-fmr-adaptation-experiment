%--------------------------------------------------------------------------
% function [event g summary] = gz_analyze_radius(gfile,maskdir,rad,fixdur)
%
% Parses .gazedata file from E-Prime Tobii system and returns gaze
% coordinates and timings with summary information.
% Uses a radius algorithm to define fixations
%
% Rejection algorithm based on validity is modelled after Tobii's manual
%
% event - data for each event
% g - data for each event sorted by condition
% summary - summary values of eye-tracking DVs
% gfile - gaze data filename
% maskdir - maskdir
% rad - fixation radius threshold
% fixdur - threshold for duration of fixations (ms)
%
% Created by Josh Goh March 2006
%--------------------------------------------------------------------------

function [event g summary] = gz_analyze_radius(gfile,maskdir,rad,fixdur)

% DATA INPUT, FORMATTING
%--------------------------------------------------------------------------
% Read gazedata file into structure variables: RTTime Xc Yc Lv Rv Trial
% Cond Stim Pic; ignore the rest

[d1 d2 d3 d4 RTTime Xc Yc d5 d6 d7 d8 d9 d10 d11 d12 Lv d13 d14 d15 d16 d17 d18 Rv Trial Cond Stim Pic]=textread(gfile,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s');

% Discard header
RTTime(1)=[];Xc(1)=[];Yc(1)=[];Lv(1)=[];Rv(1)=[];Trial(1)=[];Cond(1)=[];Stim(1)=[];Pic(1)=[];

fprintf('\n%s','Reading datapoints.......           ');
% Read into structure s
for i=1:size(RTTime,1)
    fprintf('%s',char(sprintf('\b\b\b\b\b\b\b\b\b\b\b')));
    fprintf('%05d/%5d',i,size(RTTime,1));
    s(i).RTTime = str2num(RTTime{i});
    s(i).Xc     = str2num(Xc{i});
    s(i).Yc     = str2num(Yc{i});
    s(i).Lv     = str2num(Lv{i});
    s(i).Rv     = str2num(Rv{i});
    s(i).Trial  = str2num(Trial{i});
    s(i).Cond   = Cond{i};
    s(i).Stim   = Stim{i};
    s(i).Pic    = Pic{i};
end

% Replace 'EncResources1' for unique names

if (strfind(maskdir,'omasks1')~=0)
    encrec='EncResources1\';
else
    encrec='EncResources2\';
end

for i=1:size(s,2);
    otrial{i,1}=regexprep(s(i).Pic,encrec,s(i).Stim);
    otrial{i,2}=regexprep(s(i).Pic,encrec,'');
end

% STIMULUS TIMINGS AND GAZE COORDINATES
%--------------------------------------------------------------------------

fprintf('\n%s','Processing datapoints for trials...   ');

i=1;   % datapoint counter
j=1;   % event counter
nbaddatapoints=0;

while i<=length(otrial)  % datapoint loop
    fprintf('%s',char(sprintf('\b\b\b')));
    fprintf('%03d',j);

    if ((strcmp(otrial{i,1},'')==1) || (strncmp(otrial{i,1},'F',1)==1))
        i=i+1;
    else
        % Trial onset
        event(j).onsettime      = s(i).RTTime;
        event(j).pic            = otrial{i,2};
        event(j).d(1).stimtime  = 0;
        event(j).d(1).stimptime = 0;   
        event(j).d(1).stim      = s(i).Pic;
        event(j).d(1).order     = s(i).Stim;
        event(j).d(1).sorf      = 0; % Saccade or fixation
        event(j).d(1).cd        = 0;

        % Read mask image for onset
        mask = imread(fullfile(maskdir,otrial{i,2}));
        
        while ((s(i).Lv + s(i).Rv)>=2)||(s(i).Xc>640)||(s(i).Yc>480)||(s(i).Xc<1)||(s(i).Yc<1)
            nbaddatapoints=nbaddatapoints+1;
            i=i+1;
            if i>length(otrial)
                break
            end
        end        
        if i>length(otrial)
            j=j+1;
            break
        end
        
        event(j).d(1).xcoord    = s(i).Xc; xcoordlist=s(i).Xc;
        event(j).d(1).ycoord    = s(i).Yc; ycoordlist=s(i).Yc;
        event(j).d(1).Lv        = s(i).Lv;
        event(j).d(1).Rv        = s(i).Rv;
        
        if (mask(s(i).Yc,s(i).Xc)==255)
            event(j).d(1).aoi = 'O';
        else
            event(j).d(1).aoi = 'B';
        end
        i=i+1;
        k=2;

        % Datapoints after trial onset
        while (strcmp(otrial{i-1,1},otrial{i,1})==1)
            % Ignore if rejection trial
            if ((s(i).Lv + s(i).Rv)>=2)||(s(i).Xc>640)||(s(i).Yc>480)||(s(i).Xc<1)||(s(i).Yc<1)
                nbaddatapoints=nbaddatapoints+1;
            else
                event(j).d(k).stimtime  = s(i).RTTime-event(j).onsettime;
                event(j).d(k).stimptime = s(i).RTTime-s(i-1).RTTime;
                event(j).d(k).xcoord    = s(i).Xc;
                event(j).d(k).ycoord    = s(i).Yc;
                event(j).d(k).stim      = s(i).Pic;
                event(j).d(k).order     = s(i).Stim;
                event(j).d(k).Lv        = s(i).Lv;
                event(j).d(k).Rv        = s(i).Rv;

                if (mask(s(i).Yc,s(i).Xc)==255)
                    event(j).d(k).aoi = 'O';
                else
                    event(j).d(k).aoi = 'B';
                end

                % Define saccade or fixation threshold
                % rad pixel radius, fixation duration min 100 ms based on a screen size of
                % 34  x 27  cm
                % 640 x 480 pixels
                c = sqrt((s(i).Xc-mean(xcoordlist))^2 + (s(i).Yc-mean(ycoordlist))^2); % Cartesian distance from previous coord
                event(j).d(k).cd  = c;
                xcoordlist=[xcoordlist s(i).Xc];
                ycoordlist=[ycoordlist s(i).Yc];
                if (c>rad)  % Threshold fixation pixel radius
                    event(j).d(k).sorf = 's';
                    xcoordlist=s(i).Xc; ycoordlist=s(i).Yc;
                else
                    event(j).d(k).sorf = 'f';
                end
                k=k+1;
            end
            i=i+1;
            if i>length(otrial)
                break
            end
        end % end datapoints after trial onset loop
        j=j+1;
    end
end % end datapoint loop

fprintf('\n%s%d%s','Found ',nbaddatapoints,' bad datapoints');

% Discard bad trials
k=1;
for i=1:size(event,2)
if size(event(i).d,2)>50 % Tobii 50 Hz sampling rate
temp(k)=event(i);
k=k+1;
end
end
event=temp;
nbadtrials=i-k+1;
fprintf('\n%s%d%s','Found ',nbadtrials,' bad trials');

clear temp

% EYE-MOVEMENT VARIABLES
%--------------------------------------------------------------------------
fprintf('\n%s','Calculating variables for event:datapoint...         ');

for i=1:size(event,2)
    j=1;
    fixcount=1;
    event(i).ttb  = 0; bflag=0;
    event(i).tto  = 0; oflag=0;
    event(i).pttb = 0;
    event(i).ptto = 0;
    event(i).ttio = 0;
    event(i).obs  = 0;
    event(i).bos  = 0;
    event(i).oos  = 0;
    event(i).bbs  = 0;
    event(i).cond = event(i).d(1).order;
    tcond{i}      = event(i).d(1).order;

    while j<(size(event(i).d,2)-1)

        fprintf('%s',char(sprintf('\b\b\b\b\b\b\b\b\b')));
        fprintf('%03d:%05d',i,j);

        % FIXATIONS
        % Background
        if ((strcmp(event(i).d(j).aoi,'B')==1) & (strcmp(event(i).d(j).sorf,'f')==1))
            k=1;
            fstime = event(i).d(j).stimtime; % onset time
            fx(k)=event(i).d(j).xcoord;
            fy(k)=event(i).d(j).ycoord;
            if ((oflag<1) && (strcmp(event(i).d(j+1).aoi,'B')==1))	% Time to object loop
                event(i).ptto = event(i).ptto + event(i).d(j+1).stimptime;
            end
            if ((bflag<1) && (strcmp(event(i).d(j+1).aoi,'B')==1))	% Time to background loop
                event(i).ttb = event(i).pttb + event(i).d(j+1).stimptime;
                bflag=1;
            end
            j=j+1;
            k=k+1;
            while ((j<(size(event(i).d,2)-1) && (strcmp(event(i).d(j).sorf,'f')==1)))
                fx(k)=event(i).d(j).xcoord;
                fy(k)=event(i).d(j).ycoord;
                j=j+1;
                k=k+1;
            end
            if (event(i).d(j-1).stimtime-fstime>=fixdur)
                event(i).fixlist(fixcount,1) = fstime;
                event(i).fixlist(fixcount,2) = event(i).d(j-1).stimtime-fstime; % fixation duration
                event(i).fixlist(fixcount,3) = mean(fx); % fixation centroid x coord
                event(i).fixlist(fixcount,4) = mean(fy); % fixation centroid y coord
                event(i).fixlist(fixcount,5) = 2; % fixation type; 2=bg
                fixcount=fixcount+1;
                clear fx fy;
            end
        end

        % Object
        if ((strcmp(event(i).d(j).aoi,'O')==1) & (strcmp(event(i).d(j).sorf,'f')==1))
            k=1;
            fstime = event(i).d(j).stimtime; % onset time
            fx(k) = event(i).d(j).xcoord;
            fy(k) = event(i).d(j).ycoord;
            if ((bflag<1) && (strcmp(event(i).d(j+1).aoi,'O')==1))	% Time to background loop
                event(i).pttb = event(i).pttb + event(i).d(j+1).stimptime;
            end
            if ((oflag<1) && (strcmp(event(i).d(j+1).aoi,'O')==1))	% Time to object loop
                event(i).tto = event(i).ptto + event(i).d(j+1).stimptime;
                oflag=1;
            end
            j=j+1;
            k=k+1;
            while ((j<(size(event(i).d,2)-1) && (strcmp(event(i).d(j).sorf,'f')==1)))
                fx(k) = event(i).d(j).xcoord;
                fy(k) = event(i).d(j).ycoord;
                j=j+1;
                k=k+1;
            end
            if (event(i).d(j-1).stimtime-fstime>=fixdur)
                event(i).fixlist(fixcount,1) = fstime;
                event(i).fixlist(fixcount,2) = event(i).d(j-1).stimtime-fstime; % fixation duration
                event(i).fixlist(fixcount,3) = mean(fx); % fixation mean x coord
                event(i).fixlist(fixcount,4) = mean(fy); % fixation mean y coord
                event(i).fixlist(fixcount,5) = 1; % fixation type; 1=ob
                fixcount=fixcount+1;
                clear fx fy;
            end
        end
	j=j+1;
    end

    % Fixation duration lists
    if isempty(event(i).fixlist)==1
    event(i).mofdur = [];
    event(i).nof    = 0;
    event(i).mbfdur = [];
    event(i).nbf    = 0;
    else
    obdurind=find(event(i).fixlist(:,5)==1);    
    for r=1:length(obdurind)
        obdur(r)=event(i).fixlist(obdurind(r),2);
    end

    if (exist('obdur')==1)
        event(i).ttio   = sum(obdur);
        event(i).mofdur = mean(obdur);
        event(i).nof    = length(obdurind);
        clear obdur;
    else
        event(i).mofdur = [];
        event(i).nof    = 0;
    end

    bgdurind=find(event(i).fixlist(:,5)==2);
    for r=1:length(bgdurind)
        bgdur(r)=event(i).fixlist(bgdurind(r),2);
    end

    if (exist('bgdur')==1)
        event(i).mbfdur = mean(bgdur);
        event(i).nbf    = length(bgdurind);
        clear bgdur;
    else
        event(i).mbfdur = [];
        event(i).nbf    = 0;
    end


    % SACCADES COUNTS
    for j=1:(size((event(i).fixlist),1)-1)

        % Object to Background saccades
        if ((event(i).fixlist(j,5)==1) & (event(i).fixlist(j+1,5)==2))
            event(i).obs = event(i).obs + 1;
        end

        % Background to Object saccades
        if ((event(i).fixlist(j,5)==2) & (event(i).fixlist(j+1,5)==1))
            event(i).bos = event(i).bos + 1;
        end

        % Background to Background saccades
        if ((event(i).fixlist(j,5)==2) & (event(i).fixlist(j+1,5)==2))
            event(i).bbs = event(i).bbs + 1;
        end

        % Object to Object saccades
        if ((event(i).fixlist(j,5)==1) & (event(i).fixlist(j+1,5)==1))
            event(i).oos = event(i).oos + 1;
        end
    end
    end

    i=i+1;

end


% SUMMARY MEANS AND SD
%--------------------------------------------------------------------------
summary=[];
condnames = {'OO_1','OO_2','OO_3','OO_4',...
    'ON_1','ON_2','ON_3','ON_4',...
    'NO_1','NO_2','NO_3','NO_4',...
    'NN_1','NN_2','NN_3','NN_4'};

for i=1:16
    clist = strmatch(condnames{i},tcond,'exact');
    k=1;
    q=1;
    for j=1:length(clist)
        g(i).tto(j)    = event(clist(j)).tto;
        g(i).ttb(j)    = event(clist(j)).ttb;
        g(i).ttio(j)   = event(clist(j)).ttio;
        g(i).nobs(j)   = event(clist(j)).obs;
        g(i).nbos(j)   = event(clist(j)).bos;
        g(i).nbbs(j)   = event(clist(j)).bbs;
        g(i).noos(j)   = event(clist(j)).oos;
        g(i).nbf(j)    = event(clist(j)).nbf;
        g(i).nof(j)    = event(clist(j)).nof;
        if event(clist(j)).mbfdur>0
            g(i).mbfdur(k) = event(clist(j)).mbfdur;
            k=k+1;
        end
        if event(clist(j)).mofdur>0
            g(i).mofdur(q) = event(clist(j)).mofdur;
            q=q+1;
        end
        g(i).cond{j}   = event(clist(j)).d(1).order;
        j=j+1;
    end
    j=0;

    summary(i).cond   = g(i).cond{1};

    nskipobj(i) = length(find(g(i).tto==0));
    nskipbg(i)  = length(find(g(i).ttb==0));

    summary(i).mtto    = sum(g(i).tto)/length(g(i).tto)-nskipobj(i);
    summary(i).ttosd   = std(g(i).tto);
    summary(i).mttb    = sum(g(i).ttb)/length(g(i).ttb)-nskipbg(i);
    summary(i).ttbsd   = std(g(i).ttb);
    summary(i).mttio   = mean(g(i).ttio);
    summary(i).ttiosd  = std(g(i).ttio);
    summary(i).mnobs   = mean(g(i).nobs);
    summary(i).nobssd  = std(g(i).nobs);
    summary(i).mnbos   = mean(g(i).nbos);
    summary(i).nbossd  = std(g(i).nbos);
    summary(i).mnbbs   = mean(g(i).nbbs);
    summary(i).nbbssd  = std(g(i).nbbs);
    summary(i).mnoos   = mean(g(i).noos);
    summary(i).noossd  = std(g(i).noos);
    summary(i).mnbf    = mean(g(i).nbf);
    summary(i).nbfsd   = std(g(i).nbf);
    summary(i).mnof    = mean(g(i).nof);
    summary(i).nofsd   = std(g(i).nof);
    summary(i).mbfdur  = mean(g(i).mbfdur);
    summary(i).bfdursd = std(g(i).mbfdur);
    summary(i).mofdur  = mean(g(i).mofdur);
    summary(i).ofdursd = std(g(i).mofdur);
    i=i+1;
end
i=0;


% DISPLAY SUMMARY
%--------------------------------------------------------------------------
disp([sprintf('\nCondition\tNoSkipObj\tNoSkipBg\tMeanTimetoObj\tSD\tMeanTimetoBg\tSD\tMeanTotTimeinObj\tSD\tMeanNoObjtoBgSac\tSD\tMeanNoBgtoObjSac\tSD\tMeanNoBgtoBgSac\tSD\tMeanNoObjtoObjSac\tSD\tMeanNoBgFix\tSD\tMeanNoObjFix\tSD\tMeanBgFixDur\tSD\tMeanObjFixDur\tSD')]);

r=1;
for r=1:16
    disp([sprintf('%s\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f',...
        summary(r).cond,...
        nskipobj(r),...
        nskipbg(r),...
        summary(r).mtto,...
        summary(r).ttosd,...
        summary(r).mttb,...
        summary(r).ttbsd,...
        summary(r).mttio,...
        summary(r).ttiosd,...
        summary(r).mnobs,...
        summary(r).nobssd,...
        summary(r).mnbos,...
        summary(r).nbossd,...
        summary(r).mnbbs,...
        summary(r).nbbssd,...
        summary(r).mnoos,...
        summary(r).noossd,...
        summary(r).mnbf,...
        summary(r).nbfsd,...
        summary(r).mnof,...
        summary(r).nofsd,...
        summary(r).mbfdur,...
        summary(r).bfdursd,...
        summary(r).mofdur,...
        summary(r).ofdursd)]);
    r=r+1;
end

% SAVE .CSV FILE
%--------------------------------------------------------------------------

[path name ext ver]=fileparts(gfile);
ofname = sprintf('%s/%s_summary_rad%d_fixdur%d.csv',path,name,rad,fixdur);
ofid   = fopen(ofname,'w');

fprintf(ofid,'Condition\tNoSkipObj\tMeanTimetoObj\tSD\tMeanTotTimeinObj\tSD\tMeanNoObjtoBgSac\tSD\tMeanNoBgtoObjSac\tSD\tMeanNoBgtoBgSac\tSD\tMeanNoObjtoObjSac\tSD\tMeanNoBgFix\tSD\tMeanNoObjFix\tSD\tMeanBgFixDur\tSD\tMeanObjFixDur\tSD\n');

r=1;
for r=1:16

    fprintf(ofid,'%s\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\n',...
        summary(r).cond,...
        nskipobj(r),...
        summary(r).mtto,...
        summary(r).ttosd,...
        summary(r).mttio,...
        summary(r).ttiosd,...
        summary(r).mnobs,...
        summary(r).nobssd,...
        summary(r).mnbos,...
        summary(r).nbossd,...
        summary(r).mnbbs,...
        summary(r).nbbssd,...
        summary(r).mnoos,...
        summary(r).noossd,...
        summary(r).mnbf,...
        summary(r).nbfsd,...
        summary(r).mnof,...
        summary(r).nofsd,...
        summary(r).mbfdur,...
        summary(r).bfdursd,...
        summary(r).mofdur,...
        summary(r).ofdursd);
    r=r+1;
end

fprintf(ofid,'\n\nNoofbaddatapoints-trials\t%d-%d\n\n',nbaddatapoints,nbadtrials);

fclose(ofid);






