function [s]= etagazedist(trials)

% Set parameters
trialspercond=20;
cnames={'OO_1';'OO_2';'OO_3';'OO_4';...
    'ON_1';'ON_2';'ON_3';'ON_4';...
    'NO_1';'NO_2';'NO_3';'NO_4';...
    'NN_1';'NN_2';'NN_3';'NN_4'};

% Begin loops
for i=1:length(trials) % Subject loop
    s(i).gd=zeros(length(cnames),trialspercond);s(i).nctrials=zeros(length(cnames),1);
    for j=1:length(trials(i).Trials) % Trial loop
        gd=0;
        for k=1:length(trials(i).Trials(j).d) % Datapoint loop
            gd=gd+trials(i).Trials(j).d(k).cd;
        end % End datapoint loop

        % Sort trial data into condition
        c=1;
        while c<=length(cnames)
            if strcmp(cnames(c),trials(i).Trials(j).cond)==1
                s(i).nctrials(c,1)=s(i).nctrials(c,1)+1;
                s(i).gd(c,s(i).nctrials(c,1))=gd;
            end
            c=c+1;
        end % End condition sort loop
    end % End trial loop

    % Average trial gaze distances
    for r=1:length(cnames)
        s(i).gds(r,1)=sum(s(i).gd(r,:))/s(i).nctrials(r,1);
    end
    s(i).grandmean=mean(s(i).gds(:,1));
end % End Subject loop
