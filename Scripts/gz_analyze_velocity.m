%--------------------------------------------------------------------------
% function [event g summary] = gz_analyze_velocity(gfile,maskdir,vt)
%
% Parses .gazedata file from E-Prime Tobii system and returns gaze
% coordinates and timings with summary information.
% Uses a velocity algorithm to define fixations.
% event - data for each event
% g - data for each event sorted by condition
% summary - summary values of eye-tracking DVs
% gfile - gaze data filename
% maskdir - maskdir
% vt - saccade velocity threshold
%
% Created by Josh Goh March 2006
%--------------------------------------------------------------------------

function [event g summary] = gz_analyze_velocity(gfile,maskdir,vt)

% DATA INPUT, FORMATTING
%--------------------------------------------------------------------------
% Read gazedata file into structure variables: RTTime Xc Yc Lv Rv Trial
% Cond Stim Pic; ignore the rest

[d1 d2 d3 d4 RTTime Xc Yc d5 d6 d7 d8 d9 d10 d11 d12 Lv d13 d14 d15 d16 d17 d18 Rv Trial Cond Stim Pic]=textread(gfile,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s');

% Discard header
RTTime(1)=[];Xc(1)=[];Yc(1)=[];Lv(1)=[];Rv(1)=[];Trial(1)=[];Cond(1)=[];Stim(1)=[];Pic(1)=[];

fprintf('\n%s','Reading datapoints.......           ');
% Read into structure s
for i=1:size(RTTime,1)
    fprintf('%s',char(sprintf('\b\b\b\b\b\b\b\b\b\b\b')));
    fprintf('%05d/%5d',i,size(RTTime,1));
    s(i).RTTime = str2num(RTTime{i});
    s(i).Xc     = str2num(Xc{i});
    s(i).Yc     = str2num(Yc{i});
    s(i).Lv     = str2num(Lv{i});
    s(i).Rv     = str2num(Rv{i});
    s(i).Trial  = str2num(Trial{i});
    s(i).Cond   = Cond{i};
    s(i).Stim   = Stim{i};
    s(i).Pic    = Pic{i};
    i=i+1;
end
i=0;

% Replace 'EncResources1' for unique names

if (strfind(maskdir,'omasks1') ~= 0)
encrec='EncResources1\';
else
encrec='EncResources2\';
end

for i=1:size(s,2);
    otrial{i,1}=regexprep(s(i).Pic,encrec,s(i).Stim);
    otrial{i,2}=regexprep(s(i).Pic,encrec,'');
    i=i+1;
end
i=0;

% STIMULUS TIMINGS AND GAZE COORDINATES
%--------------------------------------------------------------------------
j=1;   % event counter
for i=2:length(otrial)  % datapoint loop

    % Stim onset
    if ((strcmp(otrial{i,1},otrial{i-1,1})==0) & (strcmp(s(i).Stim,'Fixation')==0) & (strcmp(s(i).Stim,'')==0))

        % Flag if rejection trial: Validity threshold at 2, remove out of range pixels
        if (((s(i).Lv + s(i).Rv)>=2)||(s(i).Xc>640)||(s(i).Yc>480))
            event(j).reject=1;
        else

            event(j).onsettime      = s(i).RTTime;
            event(j).reject         = 0;
            event(j).pic            = otrial{i,2};
            event(j).d(1).stimtime  = 0;
            event(j).d(1).stimptime = 0;
            event(j).d(1).xcoord    = s(i).Xc;
            event(j).d(1).ycoord    = s(i).Yc;
            event(j).d(1).stim      = s(i).Pic;
            event(j).d(1).order     = s(i).Stim;
            event(j).d(1).Lv        = s(i).Lv;
            event(j).d(1).Rv        = s(i).Rv;
            event(j).d(1).sorf      = 0; % Saccade or fixation
            event(j).d(1).cd        = 0;
            event(j).d(1).vel       = 0;

            % Read mask image for onset
            mask = imread(fullfile(maskdir,otrial{i,2}));

            if (mask(s(i).Yc,s(i).Xc)==255)
                event(j).d(1).aoi = 'O';
            else
                if (mask(s(i).Yc,s(i).Xc)==0)
                    event(j).d(1).aoi = 'B';
                end
            end
        end
        j=j+1;
        k=2;
    else
        % Stim data points following onset
        if ((strcmp(s(i).Stim,'Fixation')==0) & (strcmp(s(i).Stim,'')==0))

            % Flag if rejection trial
            if (((s(i).Lv + s(i).Rv)>=2)||(s(i).Xc>640)||(s(i).Yc>480))
                event(j-1).reject=1;
            else

                event(j-1).d(k).stimtime  = s(i).RTTime-event(j-1).onsettime;
                event(j-1).d(k).stimptime = s(i).RTTime-s(i-1).RTTime;
                event(j-1).d(k).xcoord    = s(i).Xc;
                event(j-1).d(k).ycoord    = s(i).Yc;
                event(j-1).d(k).stim      = s(i).Pic;
                event(j-1).d(k).order     = s(i).Stim;
                event(j-1).d(k).Lv        = s(i).Lv;
                event(j-1).d(k).Rv        = s(i).Rv;

                if (mask(s(i).Yc,s(i).Xc)==255)
                    event(j-1).d(k).aoi = 'O';
                else
                    if (mask(s(i).Yc,s(i).Xc)==0)
                        event(j-1).d(k).aoi = 'B';
                    end
                end

                % Define saccade or fixation threshold
                % 300 degrees per second based on a screen size of
                % 34  x 27  cm
                % 640 x 480 pixels
                % Subject seated at approx 50 cm
                % tan(18.77) = (34/2)/50
                % => each pixel ~ 18.77/(640/2) = 0.06 degrees

                % Calculate eye movement velocity
                c = sqrt((s(i).Xc-s(i-1).Xc)^2 + (s(i).Yc-s(i-1).Yc)^2); % Cartesian distance from previous coord
                v = (c*0.06)/(event(j-1).d(k).stimptime/1000); % Velocity
                event(j-1).d(k).cd  = c;
                event(j-1).d(k).vel = v;

                if (v>=vt)  % Threshold velocity degrees/sec
                    event(j-1).d(k).sorf = 's';
                else
                    event(j-1).d(k).sorf = 'f';
                end
            end

            k=k+1;
        end
    end
    i=i+1;
end % end datapoint loop
i=0; j=0; k=0;


% Clean up data using Lv and Rv
fprintf('\n%s','Found bad trials......       ');

j=1;k=0;
for i=1:length(event)
    fprintf('%s',char(sprintf('\b\b\b\b\b\b\b')));
    fprintf('%03d/%03d',k,length(event));
    if (event(i).reject==0)
        f(j)=event(i);
        j=j+1;
    else
        k=k+1;
    end
    i=i+1;
end
nbadtrials=k;
i=0;
j=0;
k=0;
event=f;
clear f;


% EYE-MOVEMENT VARIABLES
%--------------------------------------------------------------------------
fprintf('\n%s','Calculating variables for event:datapoint...         ');

for i=1:size(event,2)


    j=1;
    fixcount=1;	

    event(i).ttb  = 0; bflag=0;
    event(i).tto  = 0; oflag=0;
    event(i).pttb = 0;
    event(i).ptto = 0;
    event(i).ttio = 0;
    event(i).obs  = 0;
    event(i).bos  = 0;
    event(i).oos  = 0;
    event(i).bbs  = 0;
    event(i).cond = event(i).d(1).order;
    tcond{i}      = event(i).d(1).order;

    while j<(size(event(i).d,2)-1)
    
    fprintf('%s',char(sprintf('\b\b\b\b\b\b\b\b\b')));
    fprintf('%03d:%05d',i,j);
        
	% FIXATIONS
	% Background
        if ((strcmp(event(i).d(j).aoi,'B')==1) & (strcmp(event(i).d(j).sorf,'f')==0) & (strcmp(event(i).d(j+1).sorf,'f')==1))
	    k=1;
            event(i).fixlist(fixcount,1) = event(i).d(j).stimtime; % onset time
	    fx(k) = event(i).d(j).xcoord;
	    fy(k) = event(i).d(j).ycoord;
            if ((oflag<1) && (strcmp(event(i).d(j+1).aoi,'B')==1))	% Time to object loop
            	event(i).ptto = event(i).ptto + event(i).d(j+1).stimptime;
	    end
	    if ((bflag<1) && (strcmp(event(i).d(j+1).aoi,'B')==1))	% Time to background loop
                event(i).ttb = event(i).pttb + event(i).d(j+1).stimptime;
                bflag=1;
            end
	    j=j+1;
	    k=k+1;
	    while ((j<(size(event(i).d,2)-1) && (strcmp(event(i).d(j).sorf,'f')==1)))
   	    fx(k) = event(i).d(j).xcoord;
	    fy(k) = event(i).d(j).ycoord;
	    j=j+1;
	    k=k+1;
	    end
	    event(i).fixlist(fixcount,2) = event(i).d(j-1).stimtime-event(i).fixlist(fixcount,1); % fixation duration
	    event(i).fixlist(fixcount,3) = mean(fx); % fixation centroid x coord
    	    event(i).fixlist(fixcount,4) = mean(fy); % fixation centroid y coord
	    event(i).fixlist(fixcount,5) = 2; % fixation type; 2=bg
	    fixcount=fixcount+1;
	    clear fx fy;
        end

        % Object
        if ((strcmp(event(i).d(j).aoi,'O')==1) & (strcmp(event(i).d(j).sorf,'f')==0) & (strcmp(event(i).d(j+1).sorf,'f')==1))
            k=1;
	    event(i).fixlist(fixcount,1) = event(i).d(j).stimtime; % onset time
	    fx(k) = event(i).d(j).xcoord;
	    fy(k) = event(i).d(j).ycoord;
	    if ((bflag<1) && (strcmp(event(i).d(j+1).aoi,'O')==1))	% Time to background loop
            	event(i).pttb = event(i).pttb + event(i).d(j+1).stimptime;
	    end
            if ((oflag<1) && (strcmp(event(i).d(j+1).aoi,'O')==1))	% Time to object loop
                event(i).tto = event(i).ptto + event(i).d(j+1).stimptime;
                oflag=1;
            end
	    j=j+1;
	    k=k+1;
	    while ((j<(size(event(i).d,2)-1) && (strcmp(event(i).d(j).sorf,'f')==1)))
    	    fx(k) = event(i).d(j).xcoord;
	    fy(k) = event(i).d(j).ycoord;
	    j=j+1;
	    k=k+1;
	    end
	    event(i).fixlist(fixcount,2) = event(i).d(j-1).stimtime-event(i).fixlist(fixcount,1); % fixation duration
	    event(i).fixlist(fixcount,3) = mean(fx); % fixation mean x coord
    	    event(i).fixlist(fixcount,4) = mean(fy); % fixation mean y coord
	    event(i).fixlist(fixcount,5) = 1; % fixation type; 1=ob
	    fixcount=fixcount+1;
	    clear fx fy;
        end
	j=j+1;
    end
    
    % Fixation duration lists
    obdurind=find(event(i).fixlist(:,5)==1);
    for r=1:length(obdurind)
    obdur(r)=event(i).fixlist(obdurind(r),2);
    end
    
    if (exist('obdur')==1)
    event(i).ttio   = sum(obdur);
    event(i).mofdur = mean(obdur);
    event(i).nof    = length(obdurind);
    clear obdur;
    else
    event(i).mofdur = [];
    event(i).nof    = 0;
    end
    
    bgdurind=find(event(i).fixlist(:,5)==2);
    for r=1:length(bgdurind)
    bgdur(r)=event(i).fixlist(bgdurind(r),2);
    end
    
    if (exist('bgdur')==1)
    event(i).mbfdur = mean(bgdur);
    event(i).nbf    = length(bgdurind);
    clear bgdur;
    else
    event(i).mbfdur = [];
    event(i).nbf    = 0;
    end
    
	
    % SACCADES COUNTS
    for j=1:(size((event(i).fixlist),1)-1)
    	    
    	% Object to Background saccades
    	if ((event(i).fixlist(j,5)==1) & (event(i).fixlist(j+1,5)==2))
           event(i).obs = event(i).obs + 1;
	end
	
	% Background to Object saccades
	if ((event(i).fixlist(j,5)==2) & (event(i).fixlist(j+1,5)==1))
           event(i).bos = event(i).bos + 1;
	end
	
	% Background to Background saccades
	if ((event(i).fixlist(j,5)==2) & (event(i).fixlist(j+1,5)==2))
           event(i).bbs = event(i).bbs + 1;
	end
	
	% Object to Object saccades
	if ((event(i).fixlist(j,5)==1) & (event(i).fixlist(j+1,5)==1))
           event(i).oos = event(i).oos + 1;
	end
    end

    i=i+1;

end
i=0; j=0;


% SUMMARY MEANS AND SD
%--------------------------------------------------------------------------
summary=[];
condnames = {'OO_1','OO_2','OO_3','OO_4',...
    'ON_1','ON_2','ON_3','ON_4',...
    'NO_1','NO_2','NO_3','NO_4',...
    'NN_1','NN_2','NN_3','NN_4'};

for i=1:16
    clist = strmatch(condnames{i},tcond,'exact');
    k=1;
    q=1;
    for j=1:length(clist)
        g(i).tto(j)    = event(clist(j)).tto;
	g(i).ttb(j)    = event(clist(j)).ttb;
        g(i).ttio(j)   = event(clist(j)).ttio;
        g(i).nobs(j)   = event(clist(j)).obs;
        g(i).nbos(j)   = event(clist(j)).bos;
        g(i).nbbs(j)   = event(clist(j)).bbs;
        g(i).noos(j)   = event(clist(j)).oos;
        g(i).nbf(j)    = event(clist(j)).nbf;
        g(i).nof(j)    = event(clist(j)).nof;
        if event(clist(j)).mbfdur>0
	g(i).mbfdur(k) = event(clist(j)).mbfdur;
	k=k+1;
	end
	if event(clist(j)).mofdur>0
        g(i).mofdur(q) = event(clist(j)).mofdur;
	q=q+1;
	end
        g(i).cond{j}   = event(clist(j)).d(1).order;
        j=j+1;
    end
    j=0;

    summary(i).cond   = g(i).cond{1};

    nskipobj(i) = length(find(g(i).tto==0));
    nskipbg(i)  = length(find(g(i).ttb==0));

    summary(i).mtto    = sum(g(i).tto)/length(g(i).tto)-nskipobj(i);
    summary(i).ttosd   = std(g(i).tto);
    summary(i).mttb    = sum(g(i).ttb)/length(g(i).ttb)-nskipbg(i);
    summary(i).ttbsd   = std(g(i).ttb);
    summary(i).mttio   = mean(g(i).ttio);
    summary(i).ttiosd  = std(g(i).ttio);
    summary(i).mnobs   = mean(g(i).nobs);
    summary(i).nobssd  = std(g(i).nobs);
    summary(i).mnbos   = mean(g(i).nbos);
    summary(i).nbossd  = std(g(i).nbos);
    summary(i).mnbbs   = mean(g(i).nbbs);
    summary(i).nbbssd  = std(g(i).nbbs);
    summary(i).mnoos   = mean(g(i).noos);
    summary(i).noossd  = std(g(i).noos);
    summary(i).mnbf    = mean(g(i).nbf);
    summary(i).nbfsd   = std(g(i).nbf);
    summary(i).mnof    = mean(g(i).nof);
    summary(i).nofsd   = std(g(i).nof);
    summary(i).mbfdur  = mean(g(i).mbfdur);
    summary(i).bfdursd = std(g(i).mbfdur);
    summary(i).mofdur  = mean(g(i).mofdur);
    summary(i).ofdursd = std(g(i).mofdur);
    i=i+1;
end
i=0;


% DISPLAY SUMMARY
%--------------------------------------------------------------------------
disp([sprintf('\nCondition\tNoSkipObj\tNoSkipBg\tMeanTimetoObj\tSD\tMeanTimetoBg\tSD\tMeanTotTimeinObj\tSD\tMeanNoObjtoBgSac\tSD\tMeanNoBgtoObjSac\tSD\tMeanNoBgtoBgSac\tSD\tMeanNoObjtoObjSac\tSD\tMeanNoBgFix\tSD\tMeanNoObjFix\tSD\tMeanBgFixDur\tSD\tMeanObjFixDur\tSD')]);

r=1;
for r=1:16
    disp([sprintf('%s\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f',...
        summary(r).cond,...
        nskipobj(r),...
	nskipbg(r),...
        summary(r).mtto,...
        summary(r).ttosd,...
	summary(r).mttb,...
	summary(r).ttbsd,...
        summary(r).mttio,...
        summary(r).ttiosd,...
        summary(r).mnobs,...
        summary(r).nobssd,...
        summary(r).mnbos,...
        summary(r).nbossd,...
        summary(r).mnbbs,...
        summary(r).nbbssd,...
        summary(r).mnoos,...
        summary(r).noossd,...
        summary(r).mnbf,...
        summary(r).nbfsd,...
        summary(r).mnof,...
        summary(r).nofsd,...
        summary(r).mbfdur,...
        summary(r).bfdursd,...
        summary(r).mofdur,...
        summary(r).ofdursd)]);
    r=r+1;
end

% SAVE .CSV FILE
%--------------------------------------------------------------------------

[path name ext ver]=fileparts(gfile);
ofname = sprintf('%s/%s_summary.csv',path,name);
ofid   = fopen(ofname,'w');

fprintf(ofid,'Condition\tNoSkipObj\tMeanTimetoObj\tSD\tMeanTotTimeinObj\tSD\tMeanNoObjtoBgSac\tSD\tMeanNoBgtoObjSac\tSD\tMeanNoBgtoBgSac\tSD\tMeanNoObjtoObjSac\tSD\tMeanNoBgFix\tSD\tMeanNoObjFix\tSD\tMeanBgFixDur\tSD\tMeanObjFixDur\tSD\n');

r=1;
for r=1:16
   
fprintf(ofid,'%s\t%.0f\t%.0f\t%.0f\t%.0f\t%.0f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\t%.1f\n',...
        summary(r).cond,...
        nskipobj(r),...
        summary(r).mtto,...
        summary(r).ttosd,...
        summary(r).mttio,...
        summary(r).ttiosd,...
        summary(r).mnobs,...
        summary(r).nobssd,...
        summary(r).mnbos,...
        summary(r).nbossd,...
        summary(r).mnbbs,...
        summary(r).nbbssd,...
        summary(r).mnoos,...
        summary(r).noossd,...
        summary(r).mnbf,...
        summary(r).nbfsd,...
        summary(r).mnof,...
        summary(r).nofsd,...
        summary(r).mbfdur,...
        summary(r).bfdursd,...
        summary(r).mofdur,...
        summary(r).ofdursd);
    r=r+1;
end

fprintf(ofid,'\n\nNoofbadtrials\t%d\n\n',nbadtrials);

fclose(ofid);






