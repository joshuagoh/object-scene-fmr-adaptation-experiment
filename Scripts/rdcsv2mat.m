function y=rdcsv2mat(basedir)

% basedir - Subject base directory
subjfiles=dir(fullfile(basedir,'*.csv'));

for i=1:length(subjfiles)

dfile=sprintf('%s%s',basedir,subjfiles(i).name);

[cond noskipobj meantimetoobj sd1 meantottimeinobj sd2 meannoobjtobgsac sd3 meannobgtoobjsac sd4 meannobgtobgsac sd5 meannoobjtoobjsac sd6 meannobgfix sd7 meannoobjfix sd8 meanbgfixdur sd9 meanobjfixdur sd10]=textread(dfile,'%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s');

data=[cond noskipobj meantimetoobj sd1 meantottimeinobj sd2 meannoobjtobgsac sd3 meannobgtoobjsac sd4 meannobgtobgsac sd5 meannoobjtoobjsac sd6 meannobgfix sd7 meannoobjfix sd8 meanbgfixdur sd9 meanobjfixdur sd10];

data(1,:)=[];data(17,:)=[]; data(:,1)=[] % Remove header row, last row (no. of bad trials), first column

for j=1:16
for k=1:21
y(i,j,k)=str2num(data{j,k});
k=k+1;
end
j=j+1;
end
i=i+1;
end

nskipobj=y(:,:,1);
mtimetoobj=y(:,:,2);
mtottimeinobj=y(:,:,4);
mnoobjtobgsac=y(:,:,6);
mnobgtoobjsac=y(:,:,8);
mnobgtobgsac=y(:,:,10);
mnoobjtoobjsac=y(:,:,12);
mnobgfix=y(:,:,14);
mnoobjfix=y(:,:,16);
mbgfixdur=y(:,:,18);
mobjfixdur=y(:,:,20);

save('eta','y','nskipobj','mtimetoobj','mtottimeinobj','mnoobjtobgsac','mnobgtoobjsac','mnobgtobgsac','mnoobjtoobjsac','mnobgfix','mnoobjfix','mbgfixdur','mobjfixdur');
