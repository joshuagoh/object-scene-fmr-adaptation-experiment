function etascatterplots(itype)

% set variables
lw=2;ms1=3;ms2=4;

c1=sprintf('%s-OO',itype);
c2=sprintf('%s-ON',itype);
c3=sprintf('%s-NO',itype);
c4=sprintf('%s-NN',itype);

cname={c1 c2 c3 c4};
[age culture]=textread('factors.txt','%s%s');
cf=strcat(age,culture);
fname=sprintf('rad10_fixdur30_%s.txt',itype);
v=dlmread(fname);

ye=strmatch('ye',cf);
oe=strmatch('oe',cf);
yw=strmatch('yw',cf);
ow=strmatch('ow',cf);

cond=(1:4);
cond1=cond+0.15;
cond2=cond+0.3;
cond3=cond+0.45;
i=1;

for c=1:4
    subplot(2,2,c),
    plot(cond,v((min(ye):max(ye)),i:i+3),'d','Color',[1 0 0],...
        'MarkerFaceColor',[1 0 0],...
        'MarkerSize',ms1);
    hold on;
    plot(cond,mean(v((min(ye):max(ye)),i:i+3)),'s','Color',[0 0 0],...
        'LineWidth',lw,...
        'MarkerSize',ms2);    
    plot(cond1,v((min(oe):max(oe)),i:i+3),'d','Color',[.7 0 0],...
        'MarkerSize',ms1);
    plot(cond1,mean(v((min(oe):max(oe)),i:i+3)),'s','Color',[0 0 0],...
        'LineWidth',lw,...
        'MarkerSize',ms2);
    plot(cond2,v((min(yw):max(yw)),i:i+3),'o','Color',[0 0 1],...
        'MarkerFaceColor',[0 0 1],...
        'MarkerSize',ms1);
    plot(cond2,mean(v((min(yw):max(yw)),i:i+3)),'s','Color',[0 0 0],...
        'LineWidth',lw,...
        'MarkerSize',ms2);
    plot(cond3,v((min(ow):max(ow)),i:i+3),'o','Color',[0 0 .7],...
        'MarkerSize',ms1);
    plot(cond3,mean(v((min(ow):max(ow)),i:i+3)),'s','Color',[0 0 0],...
        'LineWidth',lw,...
        'MarkerSize',ms2);

    set(gcf,'Color',[1 1 1]);
    set(gca,'XTick',[1,2,3,4],...
        'XTickLabel',{'1';'2';'3';'4'},...
        'YLim',[min(min(v)) max(max(v))]);

    hold off;
    i=i+4;
end;
saveas(gcf,itype,'bmp');
close;

clear all