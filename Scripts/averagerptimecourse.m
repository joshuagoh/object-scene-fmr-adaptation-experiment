function averagerptimecourse(ptc)

ncond=16;
nrep=16;
nbin=75;
nsubj=size(ptc,1);

ofid=fopen('ptimecoursecondrepmean_20ms.txt','w');

for i=1:nsubj
    for j=1:nbin
        aptc(i,j)=mean(ptc(i,j:nbin:size(ptc,2)));
        fprintf(ofid,'%f\t',aptc(i,j));
    end
    fprintf(ofid,'\n');
end

fclose(ofid);
