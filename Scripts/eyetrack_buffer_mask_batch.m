function eyetrack_buffer_mask_batch()

ipath = sprintf('C:/Experiments/ETA/masks2/');
opath = sprintf('C:/Experiments/ETA/omasks2/');
files = dir(ipath);

for i = 3:262
    ifile = imread(fullfile(ipath,files(i).name));
    ifile = squeeze(ifile(:,:,1));
    ifile = eyetrack_buffer_mask(ifile,20);
    ofile = uint8(ifile);
    imwrite(ofile,fullfile(opath,files(i).name));
    i = i + 1;
end