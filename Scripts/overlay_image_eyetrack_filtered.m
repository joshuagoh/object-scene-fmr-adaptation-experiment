%--------------------------------------------------------------------------
% overlay_image_eyetrack_filtered(gazedata,maskdir,ncol,nrow,pic1,pic2)
%--------------------------------------------------------------------------
%
% Overlays the x and y eye-movement coordinates for each event on the mask
% image. Coordinates based on each filtered fixations and datapoints.
%
% gazedata - output variable from gz_analyze()
% ncol - number of columns for image display
% nrow - number of rows for image display
% pic1 - start image
% pic2 - end image
% If pic1 and pic2 unspecified, default is all events with gazedata
%
% Created by Joshua on March 2006
%--------------------------------------------------------------------------

function overlay_image_eyetrack_filtered(gazedata,maskdir,ncol,nrow,pic1,pic2)

col=1;
for i=pic1:pic2
    imask = imread(fullfile(maskdir,gazedata(i).pic));
    for j=1:size(gazedata(i).fixlist,1)
        fxmask(j) = gazedata(i).fixlist(j,3);
        fymask(j) = gazedata(i).fixlist(j,4);
        j=j+1;
    end
    for j=1:size(gazedata(i).d,2)
        dxmask(j) = gazedata(i).d(j).xcoord;
        dymask(j) = gazedata(i).d(j).ycoord;
        j=j+1;
    end
    subplot(nrow,ncol,col), subimage(imask)
    hold on;
    plot(dxmask',dymask','b',dxmask',dymask','b+');
    hold on;
    plot(fxmask',fymask','r',fxmask',fymask','ro');
    hold off;
    col=col+1;
    clear fxmask fymask dxmask dymask;
end
