function [s]= etafixationdist(trials)

% Set parameters
trialspercond=20;
cnames={'OO_1';'OO_2';'OO_3';'OO_4';...
    'ON_1';'ON_2';'ON_3';'ON_4';...
    'NO_1';'NO_2';'NO_3';'NO_4';...
    'NN_1';'NN_2';'NN_3';'NN_4'};

% Begin loops
for i=1:length(trials) % Subject loop
    s(i).fd=zeros(length(cnames),trialspercond);s(i).nctrials=zeros(length(cnames),1);
    for j=1:length(trials(i).Trials) % Trial loop
        fd=0;
        if length(trials(i).Trials(j).fixlist)>=2
            for k=2:size((trials(i).Trials(j).fixlist),1) % Fixation list loop
                x1=trials(i).Trials(j).fixlist(k-1,3);
                x2=trials(i).Trials(j).fixlist(k,3);
                y1=trials(i).Trials(j).fixlist(k-1,4);
                y2=trials(i).Trials(j).fixlist(k,4);
                cfd=sqrt((x2-x1)^2+(y2-y1)^2);
                fd=fd+cfd;
            end % End datapoint loop
        end

        % Sort trial data into condition
        c=1;
        while c<=length(cnames)
            if strcmp(cnames(c),trials(i).Trials(j).cond)==1
                s(i).nctrials(c,1)=s(i).nctrials(c,1)+1;
                s(i).fd(c,s(i).nctrials(c,1))=fd;
            end
            c=c+1;
        end % End condition sort loop
    end % End trial loop

    % Average trial gaze distances
    for r=1:length(cnames)
        s(i).fds(r,1)=sum(s(i).fd(r,:))/s(i).nctrials(r,1);
    end
    s(i).grandmean=mean(s(i).fds(:,1));
end % End Subject loop
