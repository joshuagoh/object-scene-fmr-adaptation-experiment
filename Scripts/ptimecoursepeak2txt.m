function ptimecoursepeak2txt(s)

% s(2)=eos, s(1)=eys, s(4)=wos, s(3)=wys

n=1;

for i=1:length(s) % Group loop
    for j=1:length(s(i).d) % Subject loop
        for k=1:size(s(i).d(j).apnod,1) % Condition loop
	    data(n,k)=max(s(i).d(j).apnod(k,:));
	    datat(n,k)=min(find(s(i).d(j).apnod(k,:)==data(n,k)));
        end; % End condition loop
	n=n+1;
    end; % End subject loop
end; % End group loop

fid1=fopen('ptimecoursepeakval.txt','w');
fid2=fopen('ptimecoursepeaklat.txt','w');
for r=1:size(data,1)
    for c=1:size(data,2)
        fprintf(fid1,'%f\t',data(r,c));
	fprintf(fid2,'%f\t',datat(r,c));
    end;
    fprintf(fid1,'\n');
    fprintf(fid2,'\n');
end;

fclose(fid1);
fclose(fid2);
