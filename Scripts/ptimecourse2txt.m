function ptimecourse2txt(s)

% s(2)=eos, s(1)=eys, s(4)=wos, s(3)=wys

n=1;

for i=1:length(s)
    for j=1:length(s(i).d)
        m=1;
        for k=1:size(s(i).d(j).apnod,1)
            for l=1:size(s(i).d(j).apnod,2)
                data(n,m)=s(i).d(j).apnod(k,l);
                m=m+1;
            end;
        end;
        n=n+1;
    end;
end;

fid=fopen('ptimecourse_20ms.txt','w');
for r=1:size(data,1)
    for c=1:size(data,2)
        fprintf(fid,'%f\t',data(r,c));
    end;
    fprintf(fid,'\n');
end;

fclose(fid);
