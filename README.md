# Object Scene fMR-Adaptation Experiment

- Object and background scenes in naturalistic picture stimuli used in the following series of studies.
- This online repo of these stimuli was uploaded on 1 April 2022 for public sharing purposes only and will not be actively maintained.
- Issues might be addressed to joshuagoh@ntu.edu.tw. 
- Please cite at least one of these relevant works when using these stimuli.

1. Goh, J. O. S., Siong, S. C., Park, D., Gutchess, A., Hebrank, A., & Chee, M. W. L. (2004). Cortical areas involved in object, background, and object-background processing revealed with functional magnetic resonance adaptation. The Journal of Neuroscience, 24(45), 10223–10228. https://doi.org/10.1523/JNEUROSCI.3373-04.2004

2. Chee, M. W. L., Goh, J. O. S., Venkatraman, V., Tan, J. C., Gutchess, A., Sutton, B., Hebrank, A., Leshikar, E., & Park, D. (2006). Age-related changes in object processing and contextual binding revealed using fMR adaptation. Journal of Cognitive Neuroscience, 18(4), 495–507. https://doi.org/10.1162/jocn.2006.18.4.495

3. Goh, J. O., Chee, M. W., Tan, J. C., Venkatraman, V., Hebrank, A., Leshikar, E. D., Jenkins, L., Sutton, B. P., Gutchess, A. H., & Park, D. C. (2007). Age and culture modulate object processing and object-scene binding in the ventral visual area. Cognitive, Affective & Behavioral Neuroscience, 7(1), 44–52. https://doi.org/10.3758/cabn.7.1.44

4. <a id="4"></a> Goh, J. O., Tan, J. C., & Park, D. C. (2009). Culture modulates eye-movements to visual novelty. Plos One, 4(12), e8238. https://doi.org/10.1371/journal.pone.0008238

## Stimuli

- Stimuli picture files are zipped into 6 sets.
    - Set 1 Objects_PSD
    - Set 1 Scenes_PSD
    - Set 1 Stim_PSD
    - Set 2 Objects_PSD
    - Set 2 Scenes_PSD
    - Set 2 Stim_PSD
- After download and unzip.
    - *.psd graphic files (Photoshop).
    - Filenaming convention: E.g. NN01a.psd
        - First upper-case letter refers to Object quartet condition: N (changing) or O (repeated).
        - Second upper-case letter refers to Scene quartet condition: N or O.
        - Two digits refers to quartet stimuli index.
        - Last lower-case letter refers to picture index in quartet: a, b, c, d.

## Scripts

- Matlab scripts for analyzing eye-movement data as used in [[4](#4)].
